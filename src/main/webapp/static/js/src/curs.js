;(function () {

    var VALUTE_SELECTOR = '[name="valuteName"]';

    function init() {
        $.post("/valCurs/getValuteList").done(responseHandler);
    }

    function responseHandler(response) {
        initValuteSelect(response);
    }

    function initValuteSelect(response) {

        var valuteOptions = [];
        valuteOptions.push('<option label="Select valute">Select valute</option>');
        response.forEach(function (valute) {
            valuteOptions.push(buildOptions(valute))
        });

        function buildOptions(valute) {
            return '<option value="' + valute.curs + '">' + valute.charCode + '</option>';
        }

        $(VALUTE_SELECTOR).html(valuteOptions.join(""));
        $('[name="valuteName"]').on("change", showCurs);

        function showCurs(e) {
            var select = e.target;
            var value = $(select).val();
            $('#curs').text(value);
        }
    }

    init();

})();