package ru.valCurs.service;

import org.simpleframework.xml.core.Persister;
import org.springframework.stereotype.Service;
import ru.valCurs.jdo.CursValute;
import ru.valCurs.jdo.Valute;
import ru.valCurs.jdo.ValuteCodeCurs;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class TestService {
    public List<ValuteCodeCurs> getValuteList() throws Exception {
        CursValute cursValute;
        URL url = new URL("http://www.cbr.ru/scripts/XML_daily.asp");
        InputStream inputStream = url.openStream();
        Reader reader = new InputStreamReader(inputStream);
        Persister serializer = new Persister();

        cursValute = serializer.read(CursValute.class, reader, false);
        List<Valute> valuteList = cursValute.getValuteList();

        List<ValuteCodeCurs> valuteCodeCursList = new ArrayList<>();
        for (Valute valute : valuteList) {
            String value = valute.getValue();
            String value1 = value.replace(',', '.');
            String nominal = valute.getNominal();

            Double valueDouble = Double.parseDouble(value1);
            Integer nominalInt = Integer.parseInt(nominal);
            Double curs = valueDouble / nominalInt;
            ValuteCodeCurs valuteCodeCurs = new ValuteCodeCurs();
            valuteCodeCurs.setCurs(curs);
            valuteCodeCurs.setCharCode(valute.getCharCode());
            valuteCodeCursList.add(valuteCodeCurs);
        }

        return valuteCodeCursList;
    }

}
