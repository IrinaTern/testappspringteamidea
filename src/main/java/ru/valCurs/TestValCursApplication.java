package ru.valCurs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestValCursApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestValCursApplication.class, args);
	}

}
