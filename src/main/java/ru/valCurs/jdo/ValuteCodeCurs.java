package ru.valCurs.jdo;

public class ValuteCodeCurs {
    private String charCode;
    private Double curs;

    public String getCharCode() {
        return charCode;
    }

    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }

    public Double getCurs() {
        return curs;
    }

    public void setCurs(Double curs) {
        this.curs = curs;
    }
}
